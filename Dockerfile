FROM rockylinux:9

# Set local variables
ARG dir=/workdir

# Copy files
COPY Makefile requirements.txt install-deps $dir/
COPY tasks/ $dir/tasks/

# Change dir
WORKDIR $dir

# Install requirements
RUN ./install-deps

# Test
RUN make check
