all:

check:
	ansible-lint -svv .

docker:
	docker build -t ansible-debug .

.PHONY: all check
